import React, { Component } from 'react';
import AuthUserContext from './authUserContext';
import { firebaseAuth } from '../../configuration/firebase.configuration';

// ****
// WITH USER CONTEXT
// What this component do : it takes passed in component and wraps up that component
// under AuthUserContext(Place where we store our user information) by which we pass our
// authUser to the child component and then child component can user authUser property
// for its needs(take a look at Navigation where we switch between authorized menu and
// unauthorized menu )
// AuthUserContext.Provider to provide value AuthUserContext.Consumer to consume value
// ****

const withUserContext = (PassedComponent) => {
  class WithUserContext extends Component {
    constructor(props) {
      super(props);
      this.state = {
        authUser: window.localStorage.getItem('uid'),
      };
    }

    componentDidMount() {
      firebaseAuth.onAuthStateChanged((authuser) => {
        if (authuser != null) {
          window.localStorage.setItem('uid', authuser.uid);
          this.setState({ authUser: authuser });
        } else {
          window.localStorage.removeItem('uid');
          this.setState({ authUser: null });
        }
      });
    }

    render() {
      const { authUser } = this.state;
      return (
        <AuthUserContext.Provider value={authUser}> <PassedComponent />
        </AuthUserContext.Provider>);
    }
  }
  return WithUserContext;
};
export default withUserContext;
