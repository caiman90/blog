import React from 'react';
import Card, { CardTitle } from 'material-ui/Card';

const UnauthorizedPage = () => (
  <div>
    <Card>
      <CardTitle>You dont have rights to access this page</CardTitle>
    </Card>
  </div>
);

export default UnauthorizedPage;
