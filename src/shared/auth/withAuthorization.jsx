import React from 'react';
import AuthUserContext from './authUserContext';
import { firebaseAuth } from '../../configuration/firebase.configuration';
import Login from '../../components/login/Login';

// ****
// WITH AUTHORIZATION
// What this component do : it takes passed in component and authCondition function
// by which later on every time when you open component(componentDidMount) it attaches
// firebaseAuth.onAuthStateChanged and triggers it each time component is loaded
// then checks your condition with current user and then either it redirects to login
// or to unauthorized page
// It uses AuthUserContext.Consumer to consume user and redirects you to login if needed
// ****
const withAuthorization = authCondition => (Component) => {
  class WithAuthorization extends React.Component {
    constructor(props) {
      super(props);
    }

    componentDidMount() {
      firebaseAuth.onAuthStateChanged((authUser) => {
        if (!authCondition(authUser) && authUser) {
          this.props.history.push('/unauthorized');
        }
      });
    }

    render() {
      return (
        <AuthUserContext.Consumer>
          {authUser => (authUser ? <Component /> : <Login />)}
        </AuthUserContext.Consumer>
      );
    }
  }

  return WithAuthorization;
};

export default withAuthorization;
