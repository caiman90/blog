import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import MenuItems from '../MenuItems';
import PersistentDrawer from '../drawer/navigation-drawer';

export default class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Dev Tribe Balkans',
    };
  }

  render() {
    return (
      <PersistentDrawer/>
    );
  }
}

