import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import LoginButton from '../LoginButton';

export default class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'Dev Tribe',
    };
  }

  render() {
    return (
      <AppBar
        title={this.state.title}
        iconElementLeft={<IconButton><NavigationClose /></IconButton>}
        iconElementRight={<LoginButton />}
      />
    );
  }
}

