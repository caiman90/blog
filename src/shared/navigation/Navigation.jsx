import React, { Component } from 'react';
import AuthUserContext from '../auth/authUserContext';
import NavigationAuth from './NavigationAuth';
import NavigationNonAuth from './NavigationNonAuth';

export default class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <AuthUserContext.Consumer>
        {authUser => (authUser
                ? <NavigationAuth />
                : <NavigationNonAuth />)
            }
      </AuthUserContext.Consumer>
    );
  }
}

