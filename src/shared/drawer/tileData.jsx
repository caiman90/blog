// This file is shared across the demos.

import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import MailIcon from '@material-ui/icons/Mail';
import DeleteIcon from '@material-ui/icons/Delete';
import ReportIcon from '@material-ui/icons/Report';
import GroupAdd from '@material-ui/icons/GroupAdd';
import { Link } from 'react-router-dom';
import {signOut} from "../../services/authApi";

export const mailFolderListItems =(
    <div>
        <Link href to="/users" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Users" />
        </ListItem>
        </Link>

        <Link href to="/sites" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <StarIcon />
            </ListItemIcon>
            <ListItemText primary="Sites" />
        </ListItem>
        </Link>

        <Link href to="/overview" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <SendIcon />
            </ListItemIcon>
            <ListItemText primary="User Admin" />
        </ListItem>
        </Link>

        <Link href to="/account" style={{display:"flex",textDecoration:"none"}}>
        <ListItem button>
            <ListItemIcon>
                <DraftsIcon />
            </ListItemIcon>
            <ListItemText primary="Messages" />
        </ListItem>
        </Link>
        <Link to="/login" onClick={signOut} style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="Sign Out" />
            </ListItem>
        </Link>
        <Link href to="/upload" style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <GroupAdd />
                </ListItemIcon>
                <ListItemText primary="Upload Image" />
            </ListItem>
        </Link>
    </div>
);
export const notLoggedInMailFolderListItems =(
    <div>
        <Link href to="/home" style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Home" />
            </ListItem>
        </Link>

        <Link href to="/login" style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <StarIcon />
                </ListItemIcon>
                <ListItemText primary="Login" />
            </ListItem>
        </Link>

        <Link href to="/register" style={{display:"flex",textDecoration:"none"}}>
            <ListItem button>
                <ListItemIcon>
                    <GroupAdd />
                </ListItemIcon>
                <ListItemText primary="Register" />
            </ListItem>
        </Link>
    </div>
);
export const otherMailFolderListItems = (
    <div>
        <ListItem button>
            <ListItemIcon>
                <MailIcon />
            </ListItemIcon>
            <ListItemText primary="All mail" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <DeleteIcon />
            </ListItemIcon>
            <ListItemText primary="Trash" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <ReportIcon />
            </ListItemIcon>
            <ListItemText primary="Spam" />
        </ListItem>
    </div>
);