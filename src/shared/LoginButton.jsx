import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import './Shared.css';

const MenuItemLogin = () => (
    <Button color="inherit" style={{padding:"5px"}}>Login</Button>
);

export default MenuItemLogin;
