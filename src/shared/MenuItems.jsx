import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconButton from 'material-ui/IconButton';
import { signOut } from '../services/authApi';


export default class MenuItemsRight extends Component {
  constructor(props) {
    super(props);
    this.signOut = this.signOut.bind(this);
  }

  signOut() {
    signOut();
  }

  render() {
    return (
      <IconMenu
        iconButtonElement={
          <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      >
        <Link href to="/users"><MenuItem primaryText="Users" /></Link>
        <Link href to="/sites"><MenuItem primaryText="Sites" /></Link>
        <Link href to="/account"><MenuItem primaryText="Account Info" /></Link>
        <Link href to="/overview"><MenuItem primaryText="Administration" /></Link>
        <MenuItem primaryText="Sign out" onClick={this.signOut} />
      </IconMenu>
    );
  }
}
