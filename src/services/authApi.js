import { firebaseAuth, secondaryApp } from '../configuration/firebase.configuration';

// **** CREATE USER **** //

export const doCreateUserWithEmailAndPassword = (email, password) => firebaseAuth.createUserWithEmailAndPassword(email, password);

export const createUserWithEmailAndPasswordSecondInstance = (email, password) => secondaryApp.auth().createUserWithEmailAndPassword(email, password);
// **** DELETE USER **** //
export const deleteAuthUser = uid => firebaseAuth.deleteUser(uid);

// **** SIGN IN  **** //
export const doSignInWithEmailAndPassword = (email, password) => firebaseAuth.signInWithEmailAndPassword(email, password);

// **** SIGN OUT  **** //
export const signOut = () => {
  firebaseAuth.signOut();
};

// **** RESET PASSWORD  **** //
export const doPasswordReset = (email) => {
  firebaseAuth.sendPasswordResetEmail(email);
};

// **** RESET PASSWORD  **** //
export const doPasswordUpdate = (password) => {
  firebaseAuth.currentUser.updatePassword(password);
};

// **** UPDATE PRIVILEDGES
//  This is done through displayName, so we avoid call
//  to the other userDetails table
// **** //
export const updatePriviledges = (isAdmin) => {
  firebaseAuth.currentUser.updateProfile({
    displayName: isAdmin ? 'ADMIN' : 'USER',
  });
};

// **** UPDATE CONSENT
//  This is done through phoneNumber, so we avoid call
//  to the other userDetails table
// **** //
export const updateConsent = (consent) => {
  firebaseAuth.currentUser.updateProfile({
    photoURL: consent,
  });
};

// **** DELETE USER
// hit this url with UID in request body
// POST https://us-central1-geo-time-report.cloudfunctions.net/deleteUser
// ****
