import { firebaseDatabase } from '../configuration/firebase.configuration';

// **** CREATE SITE **** //
export const createSite = (site) => {
  const key = firebaseDatabase.ref('SITES').push().key;
  const path = `SITES/${key}`;
  firebaseDatabase.ref().child(path).set(site)
    .catch((error) => {
      console.log(error);
      throw (error);
    });
};

// **** DELETE SITE **** //
export const deleteSite = (key) => {
  const path = `SITES/${key}`;
  firebaseDatabase.ref().child(path).remove();
};

// **** UPDATE SITE **** //
export const updateSite = (site) => {
  const path = `SITES/${site.uid}`;
  firebaseDatabase.ref().child(path).set(site);
};

// **** GET ALL SITES **** //
export const getAllSites = () => {
  firebaseDatabase.ref('SITES').once('value').then((response) => {
    response.forEach((entry) => {
      //   const site = entry.val();
    });
  }).catch((error) => {
    alert(error);
  });
};
