import { firebaseDatabase } from '../configuration/firebase.configuration';

// **** CREATE USER
// This function is called after authApi.doCreateUserWithEmailAndPassword()
// because we have two user tables at the backend ( firebase ) one for auth
// one for user details
// ****
export const createUser = (user) => {
  const key = firebaseDatabase.ref('USERS').push().key;
  const path = `USERS/${key}`;
  user.uid = key;
  firebaseDatabase.ref().child(path).set(user)
    .catch((error) => {
      console.log(error);
      throw (error);
    });
};

// **** GET USER
// ****
export const getUser = (key) => {
  const path = `USERS/${key}`;
  firebaseDatabase.ref().child(path);
};

// **** DELETE USER
// When deleting user from USERS table delete it also from firebase auth
// ****
export const deleteUser = (key) => {
  const path = `USERS/${key}`;
  firebaseDatabase.ref().child(path).remove();
};

// **** UPDATE USER
// We are just updating userDetails object no neeed to update firebase auth
// ****
export const updateUser = (user) => {
  const path = `USERS/${user.uid}`;
  firebaseDatabase.ref().child(path).set(user);
};

// **** GET ALL USERS from USERS table **** //
export const getAllUsers = () => {
  const path = 'USERS';
  return firebaseDatabase.ref(path).once('value');
};

