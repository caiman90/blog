import firebase from 'firebase';

var config = {
    apiKey: "AIzaSyAqRVBxduQ7nIE07jmdlYBidM64SDxEWKA",
    authDomain: "personal-blog-26f0a.firebaseapp.com",
    databaseURL: "https://personal-blog-26f0a.firebaseio.com",
    projectId: "personal-blog-26f0a",
    storageBucket: "personal-blog-26f0a.appspot.com",
    messagingSenderId: "839617183717"
};
firebase.initializeApp(config);
const secondaryApp = firebase.initializeApp(config, 'Secondary');

const firebaseDatabase = firebase.database();
const firebaseAuth = firebase.auth();
const firebaseStorage = firebase.storage();

export {
    firebaseDatabase,
    firebaseAuth,
    firebaseStorage,
    secondaryApp,
};
