import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import './Login.css';
import { updatePriviledges, doSignInWithEmailAndPassword } from '../../services/authApi';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleLogin(event) {
    event.preventDefault();
    const { username, password } = this.state;
    if (username && password) {
      doSignInWithEmailAndPassword(username, password).then(() => {
          this.props.history.push("/users");
      }).catch((error) => {
        alert(error.message);
      });
    }
  }

  render() {
    return (
      <div className="loginSection">
        <p className="loginTitle">Balkan Developer Alliance</p>
        <TextField name="username" onChange={this.handleChange} /> <br />
        <TextField name="password" onChange={this.handleChange} type="password" /> <br />
        <RaisedButton
          label="Sign in"
          labelColor="white"
          backgroundColor="#30ABF7"
          className="loginButton"
          onClick={this.handleLogin}
        />
      </div>
    );
  }
}
