import React from 'react';

import AuthUserContext from '../../../shared/auth/authUserContext';
import withAuthorization from '../../../shared/auth/withAuthorization';

const AccountPage = () =>
  (<AuthUserContext.Consumer>
    {authUser =>
      (<div>
        <h1>Account: {authUser.email}</h1>
       </div>)
        }
  </AuthUserContext.Consumer>);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(AccountPage);
