
import React, { Component } from 'react';
import { Container, Row, Col } from 'react-grid-system';
import RaisedButton from 'material-ui/RaisedButton';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Create from '@material-ui/icons/Create';
import Delete from '@material-ui/icons/Delete';
import Description from '@material-ui/icons/Description';
import './Overview.css';
import axios from 'axios';
import { getAllUsers, deleteUser } from '../../../services/userApi';
import AddUser from '../addUser/addUser';
import EditUser from '../editUser/editUser';
import withAuthorization from '../../../shared/auth/withAuthorization';

class Overview extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAddOpen: false,
      isEditOpen: false,
      isDescriptionOpen: false,
      users: [],
      user: null,
    };
    this.handleEditModal = this.handleEditModal.bind(this);
    this.handleAddModal = this.handleAddModal.bind(this);
    this.handleDescriptionModal = this.handleDescriptionModal.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    getAllUsers().then((response) => {
      const data = response.val();
      const usersArray = [];

      const keys = Object.keys(data);
      keys.forEach((key) => {
        usersArray.push(data[key]);
      });
      this.setState({ users: usersArray });
    })
      .catch((error) => {
        console.log(error);
      });
  }

  componentWillUnmount(){
      this.setState( {
          isAddOpen: false,
          isEditOpen: false,
          isDescriptionOpen: false,
          users: [],
          user: null,
      });
  }

  handleAddModal(event) {
    event.preventDefault();
    this.setState({
      isAddOpen: true,
      isEditOpen: false,
      isDescriptionOpen: false,
    });
  }

  handleEditModal(user) {
    this.setState({
      isEditOpen: true,
      isAddOpen: false,
      isDescriptionOpen: false,
      user,
    });
  }

  handleDescriptionModal(event) {
    event.preventDefault();
    this.setState({
      isDescriptionOpen: true,
      isEditOpen: false,
      isAddOpen: false,

    });
  }

  handleDelete(user) {
    axios.post('https://us-central1-geo-time-report.cloudfunctions.net/deleteUser', {
      uid: user.auth_uid,
    }, {
      headers: { 'Content-Type': 'application/json' },
    }).then(() => {
      deleteUser(user.uid);
    }).catch((error) => {
      console.log(error);
    });
  }

  render() {
    return (
      <Container className="overviewConatainer" fluid style={{ lineHeight: '32px' }}>
        <br />
        <Row >
          <Col md={2}>
            Name
          </Col>
          <Col md={2} offset={{ md: 2 }} >
            Time reported
          </Col>
          <Col md={2} offset={{ md: 4 }}>
            <RaisedButton
              label="Add user"
              labelColor="white"
              backgroundColor="#30ABF7"
              fullWidth
              buttonStyle={{ borderRadius: 15 }}
              style={{ borderRadius: 15 }}
              className="addUserButton"
              icon={<PersonAdd />}
              onClick={this.handleAddModal}
            />
          </Col>
        </Row>
        <hr />
        {
          this.state.users.map((user,index) =>
            (<Row key={index}>
              <Col md={2}>
                {user.first_name}
              </Col>
              <Col md={2} offset={{ md: 2 }} >
                {user.email}
              </Col>
              <Col md={2} offset={{ md: 4 }}>
                <Description
                  onClick={this.handleDescriptionModal}
                />
                <Create  onClick={this.handleEditModal.bind(this, user)} />
                <Delete onClick={this.handleDelete.bind(this, user)} />
              </Col>
            </Row>))}
        <hr />
        <AddUser show={this.state.isAddOpen}  />
        <EditUser show={this.state.isEditOpen} user={this.state.user} />
      </Container>
    );
  }
}

const authCondition = authUser => !!authUser && authUser.displayName === 'ADMIN';
export default withAuthorization(authCondition)(Overview);
