import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import PropTypes from 'prop-types';
import { secondaryApp } from '../../../configuration/firebase.configuration';
import { createUserWithEmailAndPasswordSecondInstance } from '../../../services/authApi';
import { createUser } from '../../../services/userApi';
import UserInfoModal from '../userInfoModal/userInfoModal';


class AddUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      open: false,
      firstname: '',
      lastname: '',
      photo: '',
      phoneNumber: '',
      password: '',
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState({ open: nextProps.show });
    }
  }
  componentWillUnmount(){
        this.setState( {
            email: '',
            open: false,
            isNew: true,
            firstname: '',
            lastname: '',
            photo: '',
            phoneNumber: '',
            password: '',
        });
    }
  handleClose() {
    this.setState({ open: false });
  }

  handleChange(event) {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    const user = {};
    user.first_name = this.state.firstname;
    user.last_name = this.state.lastname;
    user.phoneNumber = this.state.phoneNumber;
    user.email = this.state.email;
    user.photo = this.state.photo;
    user.password = this.state.password;

    createUserWithEmailAndPasswordSecondInstance(this.state.email, this.state.password).then((response) => {
      user.auth_uid = response.uid;
      secondaryApp.auth().signOut();
      createUser(user);
    });
  }

  render() {
    const actions = [
      <FlatButton label="Cancel" primary onClick={this.handleClose} />,
      <FlatButton label="Submit" primary keyboardFocused onClick={this.handleSubmit} />,
    ];
    if (!this.props.show) {
      return null;
    }
    return <UserInfoModal open={this.state.open} handleChange={this.handleChange} isNew actions={actions} />;
  }
}

AddUser.propTypes = {
  show: PropTypes.bool,
};
export default AddUser;
