import React from 'react';
import { TextField } from 'material-ui';
import Dialog from 'material-ui/Dialog';
import PropTypes from 'prop-types';


const UserInfoModal = ({
  open, handleChange, isNew, actions, user,
}) => (
  <div>
      <Dialog open={open} title={isNew ? 'User information' : 'Edit user information'} actions={actions} contentStyle={{ width: 400 }} bodyStyle={{ fontSize: 14 }} autoScrollBodyContent>
        <br />
        <div>First Name:</div>
        <TextField name="firstname" defaultValue={!isNew ? user.firstname : ''} onChange={handleChange} />
        <div>Last Name: </div >
        <TextField name="lastname" defaultValue={!isNew ? user.lastname : ''} onChange={handleChange} />
        <div >Email:</div >
        <TextField name="email" onChange={handleChange} defaultValue={!isNew ? user.email : ''} />
        <div>Password</div>
        <TextField name="password" type="password" onChange={handleChange} defaultValue={!isNew ? user.password : ''} />
        <div>Photo URL:</div >
        <TextField name="photo" defaultValue={!isNew ? user.photo : ''} onChange={handleChange} />
        <div >Phone Number: </div>
        <TextField name="phoneNumber" defaultValue={!isNew ? user.phoneNumber : ''} onChange={handleChange} />
      </Dialog>
    </div >
);

UserInfoModal.propTypes = {
  open: PropTypes.bool,
  handleSubmit: PropTypes.func,
  handleChange: PropTypes.func,
  isNew: PropTypes.bool,
  handleClose: PropTypes.func,
  actions: PropTypes.array,
};

export default UserInfoModal;
