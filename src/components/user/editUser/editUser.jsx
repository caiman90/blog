import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { getUser } from '../../../services/userApi';
import { isNull } from 'util';
import { updateUser } from '../../../services/userApi';
import UserInfoModal from '../userInfoModal/userInfoModal';

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      firstname: '',
      lastname: '',
      email: '',
      photo: '',
      phoneNumber: '',
      authUid: '',
      uid: '',
      password: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps && !isNull(nextProps.user)) {
      this.setState({
        open: nextProps.show,
        firstname: nextProps.user.first_name,
        lastname: nextProps.user.last_name,
        email: nextProps.user.email,
        photo: nextProps.user.photo,
        phoneNumber: nextProps.user.phoneNumber,
        authUid: nextProps.user.auth_uid,
        uid: nextProps.user.uid,
        password: nextProps.user.password,
      });
    }
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit() {
    const user = {};
    user.first_name = this.state.firstname;
    user.last_name = this.state.lastname;
    user.phoneNumber = this.state.phoneNumber;
    user.email = this.state.email;
    user.photo = this.state.photo;
    user.auth_uid = this.state.authUid;
    user.password = this.state.password;
    user.uid = this.state.uid;
    updateUser(user);
  }

  wrapUserFields() {
    return {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      phoneNumber: this.state.phoneNumber,
      email: this.state.email,
      photo: this.state.photo,
      password: this.state.password,
    };
  }

  render() {
    const actions = [
      <FlatButton label="Cancel" primary onClick={this.handleClose} />,
      <FlatButton label="Submit" primary keyboardFocused onClick={this.handleSubmit} />,
    ];
    if (!this.props.show) {
      return null;
    }

    return (
      <UserInfoModal user={this.wrapUserFields()} open={this.state.open} handleChange={this.handleChange} isNew={false} actions={actions} />
    );
  }
}

EditUser.propTypes = {
  show: PropTypes.bool,
  user: PropTypes.object,
};

export default EditUser;

