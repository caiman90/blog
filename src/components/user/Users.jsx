import React, { Component } from 'react';
import './Users.css';
import withAuthorization from '../../shared/auth/withAuthorization';

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'This is the users page',
    };
  }

  render() {
    return (
      <div className="users">
        <header className="usersHeader">
          <h1 className="usersTitle">{this.state.title}</h1>
        </header>
      </div>
    );
  }
}
const authCondition = authUser => !!authUser && authUser.displayName === 'ADMIN';

export default withAuthorization(authCondition)(Users);
