import React from 'react';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Users from './user/Users';
import Sites from './site/Sites';
import Login from './login/Login';
import Account from './user/account/Account';
import Consent from './consent/Consent';
import Navigation from '../shared/navigation/Navigation';
import NavigationHeader from '../shared/drawer/navigation-drawer';

import Unauthorized from '../shared/auth/UnauthorizedPage';
import './App.css';
import withUserContext from '../shared/auth/withUserContext';
import Overview from './user/overview/Overview';


const App = () => (
  <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
    <BrowserRouter>
      <div>
        <NavigationHeader />
      </div>
    </BrowserRouter>
  </MuiThemeProvider>
);

export default withUserContext(App);
