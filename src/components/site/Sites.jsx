import React, { Component } from 'react';
import './Sites.css';

class Sites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: 'This is the sites page',
    };
  }

  render() {
    return (
      <div className="sites">
        <header className="sitesHeader">
          <h1 className="sitesTitle">{this.state.title}</h1>
        </header>
      </div>
    );
  }
}

export default Sites;
