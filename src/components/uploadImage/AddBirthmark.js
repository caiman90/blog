import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import  { Card, CardActions, CardContent } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton/IconButton';

import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Search from '@material-ui/icons/Search';

import { firebaseAuth,firebaseStorage,firebaseDatabase} from "../../configuration/firebase.configuration";


class AddBirthmark extends Component {
  constructor(props) {
    super(props);

    this.state = {
      crop: null,
      imageSrc: '',
      image: {},
      name: '',
      bodyLocation: ''
    };

    this.domImage = null;
  }

  handleCropChange = (crop) => {
    this.setState({ crop });
  }

  handleFileChange = (e) => {
    if(e.target.files.length > 0) {
      const file = e.target.files[0];
      const url = URL.createObjectURL(file);
      this.setState({image: file, imageName: file.name, imageSrc: url});
    }
  }

  handleInitCrop = () => {
    this.setState({
      crop: {
        x: 5,
        y: 10,
        width: 90,
        height: 80
      }
    });
  }

  handleNameChange = (e) => {
    var name = e.target.value;
    this.setState({ name: name });
  }

  handleBodyLocationChange = (e) => {
    var bodyLocation = e.target.value;
    this.setState({ bodyLocation: bodyLocation });
  }

  onImageLoaded = (image) => {
    this.domImage = image;
  }

  handleFileClick = (e) => {
    this.file.click();
  }

  saveBirthmark = () => {
      const fileExtension = this.state.image.name.split('.')[1];
      const key = firebaseDatabase.ref('birthmarks').push().key;
      const firDatPath = `birthmarks/${key}`;
      const path = `images/${key}.${fileExtension}`;

      const user = firebaseAuth.currentUser;
      const metadata = {
          customMetadata: {
              uid: user.uid,
              key: key,
              name: this.state.name,
              bodyLocation: this.state.bodyLocation,
              timestamp: new Date().getTime()
          }
      };


     firebaseStorage.ref().child(path).put(this.state.image, metadata).then((res)=>{
         res.metadata.customMetadata.fullPath=res.metadata.fullPath;
         firebaseDatabase.ref().child(firDatPath).set(res.metadata.customMetadata);
         console.log(res);

     }).catch(error => {
                  console.log(error);
                  throw (error);
              });
      //

  }

  onRenderPreviewBlob = (blob) => {
    if (blob !== null) {
      blob.name = this.state.imageName;
      blob.lastModifiedDate = new Date();
      this.setState({ image: blob })
    }
  }

  render() {
    return (
      <div className="AddBirthmark">
        <Card>
          <CardActions>
            <Button
              color="secondary"
              onClick={this.handleFileClick}
              variant="fab"
              style={{
              margin: '0 auto'
            }}>
              <PhotoCamera/>
            </Button>
          </CardActions>
          <CardContent>
            <form>
              <TextField
                fullWidth
                label="Name"
                margin="dense"
                onChange={this.handleNameChange}/>
              <TextField
                fullWidth
                label="Body Location"
                margin="dense"
                onChange={this.handleBodyLocationChange}/>
            </form>
          </CardContent>
          <CardActions>
            <div style={{
              flex: 1
            }}></div>
            <Button component={Link} to="/main">Cancel</Button>
            <Button
              color="primary"
              component={Link} to="/main"
              variant="raised"
              disabled={!this.state.image.name}
              onClick={this.saveBirthmark}>
              <Search style={{
                marginRight: '10px'
              }}/>
              Analyze
            </Button>
          </CardActions>
          <input
            accept="image/*"
            capture="environment"
            ref={file => this.file = file}
            onChange={this.handleFileChange}
            style={{
            display: 'none'
          }}
            type="file"/>
        </Card>
      </div>
    );
  }
}


export default AddBirthmark;
