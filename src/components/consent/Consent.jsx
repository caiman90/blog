import React, { Component } from 'react';
import './Consent.css';
import {firebaseStorage,firebaseDatabase,firebaseAuth} from "../../configuration/firebase.configuration";

export default class Consent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      consent: false,
      imageUrl: ''
    };

    this.handleCheck = this.handleCheck.bind(this);
    this.handleButton = this.handleButton.bind(this);
    this.getAllImages = this.getAllImages.bind(this);
  }

  handleCheck() {
    this.setState({ consent: !this.state.consent });
  }

  handleButton() {
    if (this.state.consent) this.props.history.push('/users');
  }
  getAllImages(){
      const path = 'images/-LFn1YOGaHwUw08CF0A9.jpg';
      console.log(firebaseStorage.ref().child(path));
      firebaseStorage.ref().child('images/-LFn1YOGaHwUw08CF0A9.jpg').getDownloadURL().then((res) =>{
         this.setState({imageUrl:res });
      });

  }
  render() {
    return (
      <div className="consent">
        <div className="consentBox">
          <p>
            This app will help developers through their career. We will here comment which conditions do
            companies on balkan offer from real workers insight. This site is not intended for exposing just negative
            sides of companies. Also we will provide section for advices and useful articles regarding software development
          </p>
          <div className="checkBox">
            <input
              type="checkbox"
              onChange={this.handleCheck}
              defaultChecked={this.state.consent}
            />
            <label>Yes, I do consent to the terms!</label>
          </div>
        </div>
        <button className="button" onClick={this.getAllImages}>confirm</button>
          <img src={this.state.imageUrl} alt="" />

      </div>

    );
  }
}
